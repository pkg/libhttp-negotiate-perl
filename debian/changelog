libhttp-negotiate-perl (6.01-2+apertis0) apertis; urgency=medium

  * Sync from debian/bookworm.

 -- Apertis CI <devel@lists.apertis.org>  Sat, 01 Apr 2023 02:54:16 +0000

libhttp-negotiate-perl (6.01-2) unstable; urgency=medium

  [ Debian Janitor ]
  * Bump debhelper from old 10 to 12.
  * Set debhelper-compat version in Build-Depends.
  * Set upstream metadata fields: Bug-Database, Repository, Repository-
    Browse.
  * Remove obsolete fields Contact, Name from debian/upstream/metadata
    (already present in machine-readable debian/copyright).
  * Set upstream metadata fields: Bug-Submit.

  [ gregor herrmann ]
  * debian/watch: use uscan version 4.

  [ Debian Janitor ]
  * Remove constraints unnecessary since stretch:
    + libhttp-negotiate-perl: Drop versioned constraint on libwww-perl in
      Replaces.
    + libhttp-negotiate-perl: Drop versioned constraint on libwww-perl in
      Breaks.
  * Bump debhelper from old 12 to 13.
  * Update standards version to 4.1.5, no changes needed.

 -- Jelmer Vernooĳ <jelmer@debian.org>  Tue, 06 Dec 2022 21:51:11 +0000

libhttp-negotiate-perl (6.01-1co1) apertis; urgency=medium

  [ Ritesh Raj Sarraf ]
  * debian/apertis/component: Set to development

 -- Emanuele Aina <emanuele.aina@collabora.com>  Sat, 20 Feb 2021 01:04:25 +0000

libhttp-negotiate-perl (6.01-1) unstable; urgency=medium

  [ Ansgar Burchardt ]
  * debian/control: Convert Vcs-* fields to Git.

  [ Salvatore Bonaccorso ]
  * debian/copyright: Replace DEP5 Format-Specification URL from
    svn.debian.org to anonscm.debian.org URL.

  [ Alessandro Ghedini ]
  * New upstream release
  * Bump debhelper compat level to 8
  * Bump Standards-Version to 3.9.2

  [ Salvatore Bonaccorso ]
  * Change Vcs-Git to canonical URI (git://anonscm.debian.org)
  * Change search.cpan.org based URIs to metacpan.org based URIs

  [ Axel Beckert ]
  * debian/copyright: migrate pre-1.0 format to 1.0 using "cme fix dpkg-
    copyright"

  [ gregor herrmann ]
  * debian/control: remove Nicholas Bamber from Uploaders on request of
    the MIA team.
  * Strip trailing slash from metacpan URLs.

  [ Salvatore Bonaccorso ]
  * Update Vcs-Browser URL to cgit web frontend
  * debian/control: Use HTTPS transport protocol for Vcs-Git URI

  [ gregor herrmann ]
  * debian/copyright: change Copyright-Format 1.0 URL to HTTPS.

  [ Salvatore Bonaccorso ]
  * Update Vcs-* headers for switch to salsa.debian.org

  [ gregor herrmann ]
  * Add debian/upstream/metadata.
  * Mark package as autopkgtest-able.
  * Declare compliance with Debian Policy 4.1.4.
  * Bump debhelper compatibility level to 10.
  * Add /me to Uploaders.

 -- gregor herrmann <gregoa@debian.org>  Sat, 14 Apr 2018 19:03:14 +0200

libhttp-negotiate-perl (6.00-2) unstable; urgency=low

  * Added missing Breaks and Replaces clauses in debian/control

 -- Nicholas Bamber <nicholas@periapt.co.uk>  Mon, 28 Mar 2011 18:44:18 +0100

libhttp-negotiate-perl (6.00-1) unstable; urgency=low

  * Initial Release (Closes: #619336).

 -- Nicholas Bamber <nicholas@periapt.co.uk>  Wed, 23 Mar 2011 09:07:04 +0000
